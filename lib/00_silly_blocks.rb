def reverser
  words = yield.split(' ')
  result = []
  words.each do |word|
    result << word.reverse!
  end
  result.join(' ')
end

def adder(num = 1)
  yield + num
end

def repeater(num = 1)
  num.times { yield }
end
